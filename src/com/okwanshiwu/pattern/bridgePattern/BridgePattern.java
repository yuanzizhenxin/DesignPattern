package com.okwanshiwu.pattern.bridgePattern;

/**
 * *****************************************************************************
 * 版权所有 (C)2016, 北京顺圆科技
 * <p>
 * 文件名称：桥接模式 作 者：master 完成日期：20170712 意图：将抽象部分与实现部分分离，使它们都可以独立的变化。
 * 
 * 关键代码：抽象类依赖实现类
 * **********************************************************************************************
 */
public class BridgePattern {
    public static void main(String[] args) {
        Shape redCircle = new Circle(100, 100, 10, new RedCircle());
        Shape greenCircle = new Circle(100, 100, 10, new GreenCircle());

        redCircle.draw();
        greenCircle.draw();
    }
}
