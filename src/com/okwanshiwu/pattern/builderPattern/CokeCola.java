package com.okwanshiwu.pattern.builderPattern;

public class CokeCola extends ColdDrink {

    @Override
    public float price() {
        return 3f;
    }

    @Override
    public String name() {
        return "可口可乐";
    }

}
