package com.okwanshiwu.pattern.builderPattern;

public class ChickenBurger extends Burger {

    @Override
    public float price() {
        return 10f;
    }

    @Override
    public String name() {
        return "鸡肉汉堡包";
    }

}
