package com.okwanshiwu.pattern.builderPattern;

public class MealBuilder {

    public Meal prepareVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new CokeCola());
        return meal;
    }
    
    
    public Meal prepareNonMeal(){
        return null;
    }
}
