package com.okwanshiwu.pattern.builderPattern;

public abstract class Burger implements Item {


    @Override
    public Packing packing() {
        return new Wapper();
    }
    
    @Override
    public abstract float price();

}
