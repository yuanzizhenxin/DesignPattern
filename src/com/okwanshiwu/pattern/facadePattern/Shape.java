package com.okwanshiwu.pattern.facadePattern;

public interface Shape {
    void draw();
}
