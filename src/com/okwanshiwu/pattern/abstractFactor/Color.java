package com.okwanshiwu.pattern.abstractFactor;

public interface Color {
    void fill();
}
