package com.okwanshiwu.pattern.abstractFactor;

public interface Shape {
    void draw();
}
