package com.okwanshiwu.pattern.abstractFactor;

/**
 * ***********************************************************************************************
 * 版权所有 (C)2016, 北京顺圆科技
 * <p>
 * 文件名称：抽象工厂核心类
 * 作    者： master
 * 完成日期：20170711
 ***********************************************************************************************
 */

public abstract class AbstractFactory {
    abstract Color getColor(String color);
    abstract Shape getShape(String shape) ;
 }