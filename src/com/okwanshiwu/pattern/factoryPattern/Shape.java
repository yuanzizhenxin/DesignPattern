package com.okwanshiwu.pattern.factoryPattern;

public interface Shape {
    void draw();
}
