package com.okwanshiwu.pattern.factoryPattern;

/**
 * ***********************************************************************************************
 * 版权所有 (C)2016, 北京顺圆科技
 * <p>
 * 文件名称：工厂模式简单介绍及代码
 * 作    者： master
 * 完成日期：20170711
 * 意图：         定义一个创建对象的接口，让其子类自己决定实例化哪一个工厂类，工厂模式使其创建过程延迟到子类进行。
 * 主要解决：主要解决接口选择的问题。
 * 何时使用：我们明确地计划不同条件下创建不同实例时。
 * 如何解决：让其子类实现工厂接口，返回的也是一个抽象的产品
 * 使用场景： 
 * 1、日志记录器：记录可能记录到本地硬盘、系统事件、远程服务器等，用户可以选择记录日志到什么地方。 
 * 2、数据库访问，当用户不知道最后系统采用哪一类数据库，以及数据库可能有变化时。 
 * 3、设计一个连接服务器的框架，需要三个协议，"POP3"、"IMAP"、"HTTP"，可以把这三个作为产品类，共同实现一个接口。
 * 注意事项：作为一种创建类模式，在任何需要生成复杂对象的地方，都可以使用工厂方法模式。有一点需要注意的地方就是复杂对象适合使用工厂模式，而简单对象，
 * 特别是只需要通过 new 就可以完成创建的对象，无需使用工厂模式。如果使用工厂模式，就需要引入一个工厂类，会增加系统的复杂度。
 * 
 ***********************************************************************************************
 */
public class FactoryPattern {
    
    
    //使用 getShape 方法获取形状类型的对象
    public Shape getShape(String shapeType){
       if(shapeType == null){
          return null;
       }     
       if(shapeType.equalsIgnoreCase("CIRCLE")){
          return new Circle();
       } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
          return new Rectangle();
       } else if(shapeType.equalsIgnoreCase("SQUARE")){
          return new Square();
       }
       return null;
    }
    
}
