package com.okwanshiwu.pattern.ProxyPattern;

public interface Image {
    void display();
}