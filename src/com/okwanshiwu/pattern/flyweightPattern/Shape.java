package com.okwanshiwu.pattern.flyweightPattern;

public interface Shape {
    void draw();
}
