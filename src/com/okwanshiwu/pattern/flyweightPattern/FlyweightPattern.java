package com.okwanshiwu.pattern.flyweightPattern;

/**
 * *****************************************************************************
 * 版权所有 (C)2016, 北京顺圆科技
 * <p>
 * 文件名称：享元模式 作 者： master 完成日期：20170713 享元模式（Flyweight
 * Pattern）主要用于减少创建对象的数量，以减少内存占用和提高性能 优点：大大减少对象的创建，降低系统的内存，使效率提高。
 * 缺点：提高了系统的负责度，需要分离出外部状态和内部状态，而且外部状态具有固有化的性质，不应该随着内部状态的变化而变化，否则会造成系统的混乱。 使用场景：
 * 1、系统有大量相似对象。 2、需要缓冲池的场景。 注意事项： 1、注意划分外部状态和内部状态，否则可能会引起线程安全问题。
 * 2、这些类必须有一个工厂对象加以控制。
 * 实现
 * 我们将创建一个 Shape 接口和实现了 Shape 接口的实体类 Circle。下一步是定义工厂类 ShapeFactory。
 * ShapeFactory 有一个 Circle 的 HashMap，其中键名为 Circle 对象的颜色。无论何时接收到请求，都会创建一个特定颜色的圆。
 * ShapeFactory 检查它的 HashMap 中的 circle 对象，如果找到 Circle 对象，则返回该对象，否则将创建一个存储在 hashmap 中以备后续使用的新对象，并把该对象返回到客户端
 * *****************************************************************************
 */
public class FlyweightPattern {
    private static final String colors[] = { "Red", "Green", "Blue", "White", "Black" };

    public static void main(String[] args) {
        for (int i = 0; i < 20; ++i) {
            Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());
            circle.setX(getRandomX());
            circle.setY(getRandomY());
            circle.setRadius(100);
            circle.draw();
        }
    }

    private static String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    private static int getRandomX() {
        return (int) (Math.random() * 100);
    }

    private static int getRandomY() {
        return (int) (Math.random() * 100);
    }

}
