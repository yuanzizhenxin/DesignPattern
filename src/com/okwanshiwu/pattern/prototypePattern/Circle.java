package com.okwanshiwu.pattern.prototypePattern;

public class Circle extends Shape {

    public Circle() {
        type = "Circle";
    }

    @Override
    void draw() {
        System.out.println(type);
    }

}
