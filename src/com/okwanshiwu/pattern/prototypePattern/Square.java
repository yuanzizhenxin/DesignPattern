package com.okwanshiwu.pattern.prototypePattern;

public class Square extends Shape {
    
    
    
    public Square() {
        type = "Square";
    }

    @Override
    void draw() {
        System.out.println(type);
    }

}
