package com.okwanshiwu.pattern.prototypePattern;
/**
 * ***********************************************************************************************
 * 版权所有 (C)2016, 北京顺圆科技
 * <p>
 * 文件名称：原型模式
 * 作    者： master
 * 完成日期：20170712
 * 原型模式（Prototype Pattern）是用于创建重复的对象，同时又能保证性能。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。
 * 在运行期建立和删除原型。
 * 
 * **********************************************************************************************
 */
public class PrototypePattern {
    
    public static void main(String[] args) {
        ShapeCache.loadCache();

        Shape clonedShape = (Shape) ShapeCache.getShape("1");
        System.out.println("Shape : " + clonedShape.getType());       

        Shape clonedShape2 = (Shape) ShapeCache.getShape("2");
        System.out.println("Shape : " + clonedShape2.getType());      
   
     }
    
}
