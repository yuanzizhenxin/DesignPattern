package com.okwanshiwu.pattern.decoratorPattern;

public abstract class ShapeDecorator implements Shape{
    
    protected Shape shape;
    
    
    
    public ShapeDecorator(Shape shape) {
        super();
        this.shape = shape;
    }

    @Override
    public void draw() {
        shape.draw();
    }

}
