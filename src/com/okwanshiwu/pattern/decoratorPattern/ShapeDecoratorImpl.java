package com.okwanshiwu.pattern.decoratorPattern;

public class ShapeDecoratorImpl extends ShapeDecorator{

    public ShapeDecoratorImpl(Shape shape) {
        super(shape);
    }
    
    @Override
    public void draw() {
        System.out.println("执行前");
        shape.draw();
        System.out.println("执行后");
    
    }
    
}
