package com.okwanshiwu.pattern.decoratorPattern;
/**
 * ***********************************************************************************************
 * 版权所有 (C)2016, 北京顺圆科技
 * <p>
 * 文件名称：装饰者模式
 * 作    者：master 
 * 完成日期：20170711
 * 装饰器模式（Decorator Pattern）允许向一个现有的对象添加新的功能，同时又不改变其结构。
 * **********************************************************************************************
 */
public class DecoratorPattern {
    
    
    public static void main(String[] args) {
        Shape redCircle = new ShapeDecoratorImpl(new Circle());
        redCircle.draw();
    }
    
}
