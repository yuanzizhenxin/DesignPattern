package com.okwanshiwu.pattern.decoratorPattern;

public interface Shape {
    void draw();
}
