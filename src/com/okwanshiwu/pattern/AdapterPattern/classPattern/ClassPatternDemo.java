package com.okwanshiwu.pattern.AdapterPattern.classPattern;

public class ClassPatternDemo {
    
    public static void main(String[] args) {
        ReadyClass rc = new ReadyAdapter();
        rc.shuo();
        ((ReadyAdapter)rc).dou();
        ((ReadyAdapter)rc).xue();
        ((ReadyAdapter)rc).chang();
    }
    
}
