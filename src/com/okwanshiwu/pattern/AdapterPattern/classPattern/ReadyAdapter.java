package com.okwanshiwu.pattern.AdapterPattern.classPattern;

public class ReadyAdapter extends ReadyClass implements NewAddClass{

    @Override
    public void xue() {
        System.out.println("我会学了");
    }

    @Override
    public void dou() {
        System.out.println("我会逗了");
    }

    @Override
    public void chang() {
        System.out.println("我会唱了");
    }
    
}
