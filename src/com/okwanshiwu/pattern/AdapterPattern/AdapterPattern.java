package com.okwanshiwu.pattern.AdapterPattern;
/**
 * ***********************************************************************************************
 * 版权所有 
 * <p>
 * 文件名称：适配器模式
 * 作    者： master
 * 完成日期：20170712
 * 适配器模式（Adapter Pattern）是作为两个不兼容的接口之间的桥梁。这种类型的设计模式属于结构型模式，它结合了两个独立接口的功能。
 * 主要解决：主要解决在软件系统中，常常要将一些"现存的对象"放到新的环境中，而新环境要求的接口是现对象不能满足的
 * 如何解决：继承或依赖（推荐）
 * 注意事项：适配器不是在详细设计时添加的，而是解决正在服役的项目的问题
 * 
 * 实现
 * 我们有一个 MediaPlayer接口和一个实现了 MediaPlayer接口的实体类 AudioPlayer。默认情况下，AudioPlayer可以播放 mp3格式的音频文件。
 * 我们还有另一个接口 AdvancedMediaPlayer和实现了 AdvancedMediaPlayer接口的实体类。该类可以播放 vlc和 mp4格式的文件。
 * 我们想要让 AudioPlayer播放其他格式的音频文件。为了实现这个功能，我们需要创建一个实现了 MediaPlayer 接口的适配器类 MediaAdapter，并使用 AdvancedMediaPlayer对象来播放所需的格式。
 * AudioPlayer使用适配器类 MediaAdapter传递所需的音频类型，不需要知道能播放所需格式音频的实际类。AdapterPatternDemo，我们的演示类使用 AudioPlayer类来播放各种格式。
 * 
 ***********************************************************************************************
 */
public class AdapterPattern {
    
    
    
    
}
